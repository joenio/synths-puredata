# synths-puredata

puredata synths and effects for live coding and recording experiences, with
main focus on musical techniques for dub

## puredata dub siren synth

puredata patch made by indizivel, dependes on pd-external ELSE

see at `dub-siren/DUB_SIREN.pd`

works with dublang nvim `fudi` plugin, example:

start puredata without GUI
```sh
puredata -nogui dub-siren/DUB_SIREN.pd
```

then use from dublang with the `fudi` plugin

```dublang
#!fudi

onoff 1;

onoff 0;

onoff 1; freq 440;
```

## oscillators

at the `oscilattors/` directory can be found simple oscillators to be used in
combination with puredata-repl that is based on top of node-libpd

- https://codeberg.org/joenio/puredata-repl
- https://github.com/ircam-ismm/node-libpd

oscillators available
- sine.pd
- sawtooth.pd
- square.pd

example using sine wave oscillator from dublang
```dublang
#!puredata

open /home/joenio/art/sounds/synths-puredata/oscillator/sine.pd
send /home/joenio/art/sounds/synths-puredata/oscillator/sine.pd freq 50
send /home/joenio/art/sounds/synths-puredata/oscillator/sine.pd vol 0.5
```

### dub siren arguments

| Argument | Values |
| -------- | ------ |
| onoff    | `[0 "off", 1 "on"]` |
| freq     | `0-2500` |
| delay    | `[0 "off", 1 "on"]` |
| time     | `0-2000` |
| wave     | `[0 "tri", 1 "ramp", 2 "saw", 3 "sqr"]` |
| mod      | `0-200` (freq modulation) |
| vol      | `0-1` |
| time     | `0-2000` |

## author

Joenio Marques da Costa <<joenio@joenio.me>>
